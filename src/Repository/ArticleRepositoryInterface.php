<?php

namespace App\Repository;

interface ArticleRepositoryInterface
{
    public function findAll();
}