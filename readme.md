#### Requirements
<ul>  
<li> Docker </li>  
<li> Docker-compose</li>  
</ul>  

#### Installation
`` 1- docker-compose build up ``<br>
`` 2- docker-compose up -d ``<br>
`` 3- composer install ``<br>

<br>  

### Routing:
<b>Base URL:</b>  
```http://localhost:8001```